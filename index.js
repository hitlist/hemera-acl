'use strict'

/**
 * Hemera pluging for coarse role based or granular permission based
 * access control
 * @module @allstar/hemera-permission
 * @author Eric Satterwhite
 * @since 1.0.0
 **/
const HP = require('hemera-plugin')
const SuperError = require('super-error')
const acl = require('./lib/acl')

// emulate HemeraErrors
const HemeraError = SuperError.subclass('HemeraError')

module.exports = HP(hemeraPermission, {
  hemera: '>=5'
, name: 'hemera-acl'
, options: {
    separator: undefined
  }
, scoped: false
})

function hemeraPermission(hemera, opts, done) {
  hemera.ext('onServerPreHandler', (ctx, req, res, next) => {
    const auth = ctx.matchedAction.schema.auth$
    if (!auth) return next()
    const {roles, permissions} = auth
    const user = ctx.meta$.user || {}

    if (roles || permissions) {
      if (roles) {
        if (acl.hasRole(user, roles)) {
          ctx.user$ = user
          return next()
        }
      }

      if (permissions) {
        if (acl.hasPerm(user, permissions, opts.separator)) {
          ctx.user$ = user
          return next()
        }
      }
      const error = new HemeraError({
        message: 'No Matching permissions'
      , code: 'EAUTH'
      , name: 'NotAuthorized'
      , status: 401
      })
      return next(error)
    }
    next()
  })
  done()
}
