'use strict'

const {test, threw} = require('tap')
const nats = require('nats')
const Hemera = require('nats-hemera')
const testCase = require('../test-case')
const acl = require('../../')
const nc = nats.connect({
  servers: ['nats://0.0.0.0:4222']
})

test('hemera-permissions', (t) => {
  let hemera = null
  t.on('end', () => {
    hemera && hemera.close()
  })
  t.test('setup', (tt) => {
    hemera = new Hemera(nc)
    hemera.use(acl)
    hemera.ready(tt.end)
  })

  testCase(t, {
    name: 'success'
  , description: 'role match'
  }, (tt) => {
    tt.plan(3)
    hemera.add({
      topic:'acl'
    , cmd: 'role'
    , auth$: {
        roles: ['blogger', 'admin']
      }
    }, function(req, cb) {
      tt.match(this.user$, {
        roles: ['admin']
      , permissions:{
          blog: {
            post: {
              create: true
            }
          }
        }
      }, 'context user$ object')
      cb(null, 'done')
    })

    hemera.act({
      topic: 'acl'
    , cmd: 'role'
    , meta$: {
        user: {
          roles: ['admin']
        , permissions:{
            blog: {
              post: {
                create: true
              }
            }
          }
        }
      }
    }, function(err, resp) {
      tt.error(err)
      tt.match(resp, /done/)
    })
  })

  testCase(t, {
    name: 'unauthorized'
  , description: 'no matching role'
  }, (tt) => {
    tt.plan(2)
    hemera.add({
      topic: 'acl'
    , cmd: 'norole'
    , auth$: {
        roles: 'admin'
      }
    }, function(req, cb) {
      cb(new Error('failed'))
      tt.fail('authorized actor called')
    })

    hemera.act({
      topic: 'acl'
    , cmd: 'norole'
    , meta$: {
        user: { roles: ['fake'] }
      }
    }, function(err, resp) {
      tt.type(err, Error)
      tt.equal(err.code, 'EAUTH')
    })
  })

  testCase(t, {
    name: 'unauthorized'
  , description: 'route empty roles'
  }, (tt) => {
    tt.plan(5)
    hemera.add({
      topic: 'acl'
    , cmd: 'emptyrole'
    , auth$: {
        roles: []
      }
    }, function(req, cb) {
      if (!this.user$.superuser) {
        cb(new Error('failed'))
        tt.fail('authorized actor called')
      }

      tt.ok(this.user$.superuser)
      cb(null, 'superuser')
    })

    hemera.act({
      topic: 'acl'
    , cmd: 'emptyrole'
    , meta$: {
        user: { roles: ['fake'] }
      }
    }, function(err, resp) {
      tt.type(err, Error)
      tt.equal(err.code, 'EAUTH')
    })
    hemera.act({
      topic: 'acl'
    , cmd: 'emptyrole'
    , meta$: {
        user: { roles: ['fake'] , superuser: true}
      }
    }, function(err, resp) {
      tt.error(err)
      tt.match(resp, /superuser/i, 'match super user')
    })
  })
  testCase(t, {
    code: 'success'
  , description: 'deep permission match'
  }, (tt) => {
    tt.plan(3)
    hemera.add({
      topic: 'acl'
    , cmd: 'perms'
    , auth$: {
        permissions: 'test:acl:pass'
      }
    }, function(req, cb){
      tt.match(this.user$, {
        permissions: {test: {acl: {pass: true}}}
      })
      cb(null, 'permission match')
    })

    hemera.act({
      topic: 'acl'
    , cmd: 'perms'
    , meta$: {
        user: {
          permissions: {
            test: {
              acl: {
                pass: true
              }
            }
          }
        }
      }
    }, function(err, resp) {
      tt.error(err)
      tt.match(resp, /permission match/, 'response')
    })
  })

  testCase(t, {
    code: 'failure'
  , description: 'permission mis match'
  }, (tt) => {
    tt.plan(2)
    hemera.add({
      topic: 'acl'
    , cmd: 'noperms'
    , auth$: {
        permissions: 'test:acl:fail'
      }
    }, function(req, cb) {
      tt.fail('permission missmatch matched')
      cb(new Error('Perms Fail'))
    })
    hemera.act({
      topic: 'acl'
    , cmd: 'noperms'
    , meta$: {
        user: {
          permisions: {
            auth: 1
          }
        }
      }
    }, function(err, resp) {
      tt.type(err, Error)
      tt.equal(err.code, 'EAUTH', 'error code === EAUTH')
    })
  })

  testCase(t, {
    code: 'success'
  , description: 'no auth$ comands pass'
  }, (tt) => {
    tt.plan(3)
    hemera.add({
      topic: 'acl'
    , cmd: 'noauth'
    }, function(req, cb) {
      cb(null, 'success')
      tt.ok('unauthorised called')
    })
    hemera.act({
      topic: 'acl'
    , cmd: 'noauth'
    , meta$: {}
    }, function(err, res) {
      tt.error(err)
      tt.match(res, /success/i)
    })
  })

  testCase(t, {
    code: 'success'
  , description: 'auth$, no roles, no permissions'
  }, (tt) => {
    tt.plan(3)
    hemera.add({
      topic: 'acl'
    , cmd: 'missingauth'
    , auth$: {}
    }, function(req, cb) {
      cb(null, 'missingauth')
      tt.ok('unauthorised called')
    })
    hemera.act({
      topic: 'acl'
    , cmd: 'missingauth'
    , meta$: {
        user: {roles: [], permissions: {}}
      }
    }, function(err, res) {
      tt.error(err)
      tt.match(res, /missingauth/i)
    })
  })
  t.end()
}).catch(threw)
