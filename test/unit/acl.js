'use strict'

const {test} = require('tap')
const testCase = require('../test-case')
const acl = require('../../lib/acl')

const users = new Map([
  ['one', {}]
, ['two', {
    roles: ['admin', 'blogger']
  , permissions: {
      auth: {
        user: {
          create: true
        , 'delete': false
        , update: true
        , read: true
        , fake: undefined
        }
      }
    , test: 1
    , blog: {
        post: {
          create: true
        , 'delete': true
        , update: true
        , read:  true
        }
      }
    }
  }]
, ['three', {
    superuser: true
  }]
])

test('acl', (t) => {
  t.equal(Object.keys(acl).length, 2, 'exports two functions')
  t.type(acl.hasPerm, 'function', 'hasPerm is a function')
  t.type(acl.hasRole, 'function', 'hasRole is a function')
  t.end()
})

test('hasPerm', (t) => {
  testCase(t, {
    name: 'superuser'
  , description: 'always return true'
  }, (tt) => {
    const user = users.get('three')
    tt.ok(acl.hasPerm(user, 'fake'), 'fake === true')
    tt.ok(acl.hasPerm(user, 'fake:foo'), 'fake:foo === true')
    tt.ok(acl.hasPerm(user, 'fake:foo:bar'), 'fake:foo:bar === true')
    tt.notOk(acl.hasPerm(null, 'any'), 'user===null')
    tt.end()
  })

  testCase(t, {
    name: 'permissions'
  , descriptions: 'handle missing permission objects'
  }, (tt) => {
    const user = users.get('one')
    tt.notOk(acl.hasPerm(user, 'any'), 'any === false')
    tt.end()
  })

  testCase(t, {
    name: 'permissions'
  , description: 'nested permission lookups'
  }, (tt) => {
    const user = users.get('two')
    tt.ok(acl.hasPerm(user, 'auth'), 'auth')
    tt.ok(acl.hasPerm(user, 'auth:user:create'), 'auth:user:create')
    tt.ok(acl.hasPerm(user, 'auth-user-create', '-'), 'customer separator -')
    tt.notOk(acl.hasPerm(user, 'auth:user:delete'), 'auth:user:delete')
    tt.ok(acl.hasPerm(user, 'blog:post:create'), 'blog:post:create')
    tt.notOk(acl.hasPerm(user, 'fake'), 'fake')
    tt.notOk(acl.hasPerm(user, 'auth:user:fake'), 'auth:user:fake')
    tt.ok(acl.hasPerm(user, 'test:doesnt:matter'), 'terminal permission condition')
    tt.end()
  })
  t.end()
})

test('hasRole', (t) => {
  testCase(t, {
    name: 'superuser'
  , description: 'always returns true'
  }, (tt) => {
    const user = users.get('three')
    tt.ok(acl.hasRole(user, 'fake'), 'fake')
    tt.ok(acl.hasRole(user, null), 'null')
    tt.notOk(acl.hasRole(), 'no user')
    tt.end()
  })
  testCase(t, {
    name: 'roles'
  , description: 'should find multiple role types'
  }, (tt) => {
    const user = users.get('two')
    tt.ok(acl.hasRole(user, 'admin'), 'admin === true')
    tt.ok(acl.hasRole(user,  'blogger'), 'blogger === true')
    tt.notOk(acl.hasRole(null, 'test'), 'no user')
    tt.notOk(acl.hasRole(user, null), 'no role')
    tt.notOk(acl.hasRole({roles: true}, 'admin'), 'roles=tue')
    tt.end()
  })
  t.end()
})
