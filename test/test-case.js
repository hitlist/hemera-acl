'use strict'

const tap = require('tap')

if (require.main === module) return tap.pass('skipping test suite')

module.exports = testCase

function testCase(test, opts, fn) {
  const {name, description} = opts
  test.test(`(${name}) ${description}`, fn)
}
