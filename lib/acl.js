'use strict'

module.exports = {
  hasPerm
, hasRole
}

function hasPerm(user, prop, sep = ':'){
  if (!user) return false
  if (user.superuser === true) return true
  let perms = user.permissions
  if (!perms) return false
  let parts = prop.split(sep)
  const last = parts.pop()
  let current = null
  while (current = parts.shift()) {
    perms = perms[current]
    if (typeof perms !== 'object'){
       return !!perms
    }
  }
  return !!perms[last]
}

function hasRole(user, role) {
  if (!user) return false
  if (user.superuser === true) return true
  if (!role) return false
  if (!Array.isArray(user.roles) || !user.roles.length) return false
  return intersect(user.roles, Array.isArray(role) ? role: [role])
}

function intersect(user_roles, oneof_roles) {
  if (!oneof_roles.length) return false

  for (const role of oneof_roles) {
    if (user_roles.includes(role)) return true
  }
  return false
}
